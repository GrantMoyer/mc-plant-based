[CC0]

To the extent possible under law, Grant Moyer has waived all copyright and related or neighboring rights to Plant Based.

[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
