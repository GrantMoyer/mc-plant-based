package com.grantmoyer.mcplantbased.mixin;

import net.minecraft.item.Item;
import net.minecraft.potion.Potion;
import net.minecraft.recipe.BrewingRecipeRegistry;
import org.spongepowered.asm.mixin.gen.Invoker;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(BrewingRecipeRegistry.class)
public interface MixinBrewingRecipeRegistry {
	@Invoker("registerPotionRecipe")
	public static void invokeRegisterPotionRecipe(Potion input, Item item, Potion output) {
		throw new AssertionError();
	}

	@Invoker("registerItemRecipe")
	public static void invokeRegisterItemRecipe(Item input, Item item, Item output) {
		throw new AssertionError();
	}
}

