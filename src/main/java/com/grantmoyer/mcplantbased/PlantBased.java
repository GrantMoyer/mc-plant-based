package com.grantmoyer.mcplantbased;

import com.grantmoyer.mcplantbased.mixin.MixinBrewingRecipeRegistry;
import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.potion.Potion;
import net.minecraft.potion.Potions;
import net.minecraft.recipe.BrewingRecipeRegistry;

public class PlantBased implements ModInitializer {
	private static class PotionRecipe {
		public Potion input;
		public Item item;
		public Potion output;

		public PotionRecipe(Potion output, Potion input, Item item) {
			this.input = input;
			this.item = item;
			this.output = output;
		}
	}

	private static class ItemRecipe {
		public Item input;
		public Item item;
		public Item output;

		public ItemRecipe(Item output, Item input, Item item) {
			this.input = input;
			this.item = item;
			this.output = output;
		}
	}

	public static final PotionRecipe[] POTION_RECIPES = {
		new PotionRecipe(Potions.HARMING, Potions.HEALING, Items.MUSHROOM_STEW),
		new PotionRecipe(Potions.HARMING, Potions.POISON, Items.MUSHROOM_STEW),
		new PotionRecipe(Potions.STRONG_HARMING, Potions.STRONG_HEALING, Items.MUSHROOM_STEW),
		new PotionRecipe(Potions.STRONG_HARMING, Potions.STRONG_POISON, Items.MUSHROOM_STEW),

		new PotionRecipe(Potions.INVISIBILITY, Potions.NIGHT_VISION, Items.GLASS),
		new PotionRecipe(Potions.LONG_INVISIBILITY, Potions.LONG_NIGHT_VISION, Items.GLASS),

		new PotionRecipe(Potions.POISON, Potions.AWKWARD, Items.BROWN_MUSHROOM),
		new PotionRecipe(Potions.POISON, Potions.AWKWARD, Items.RED_MUSHROOM),

		new PotionRecipe(Potions.SLOWNESS, Potions.LEAPING, Items.SNOWBALL),
		new PotionRecipe(Potions.SLOWNESS, Potions.SWIFTNESS, Items.SNOWBALL),
		new PotionRecipe(Potions.LONG_SLOWNESS, Potions.LONG_LEAPING, Items.SNOWBALL),
		new PotionRecipe(Potions.LONG_SLOWNESS, Potions.LONG_SWIFTNESS, Items.SNOWBALL),

		new PotionRecipe(Potions.WATER_BREATHING, Potions.AWKWARD, Items.KELP),

		new PotionRecipe(Potions.WEAKNESS, Potions.WATER, Items.ORANGE_TULIP),
		new PotionRecipe(Potions.WEAKNESS, Potions.WATER, Items.PINK_TULIP),
		new PotionRecipe(Potions.WEAKNESS, Potions.WATER, Items.RED_TULIP),
		new PotionRecipe(Potions.WEAKNESS, Potions.WATER, Items.WHITE_TULIP),
	};

	public static final ItemRecipe[] ITEM_RECIPES = {
		new ItemRecipe(Items.HONEY_BOTTLE, Items.GLASS_BOTTLE, Items.BEETROOT_SOUP),
	};

	@Override
	public void onInitialize() {
		for (PotionRecipe recipe : POTION_RECIPES) {
			MixinBrewingRecipeRegistry.invokeRegisterPotionRecipe(recipe.input, recipe.item, recipe.output);
		}
		for (ItemRecipe recipe : ITEM_RECIPES) {
			MixinBrewingRecipeRegistry.invokeRegisterItemRecipe(recipe.input, recipe.item, recipe.output);
		}
		System.out.println(String.format(
			"Plant Based registered %d brewing recipes",
			POTION_RECIPES.length + ITEM_RECIPES.length
		));
	}
}
